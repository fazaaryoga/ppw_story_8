from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from .views import *
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

# Create your tests here.

class PPW_Story_8_UnitTest(TestCase):
    
    def testIndexPageExist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def testIndexPageUseIndexHTML(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testIndexPageUsesIndexHTML(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class PPW_Story_8_FunctionalTest(StaticLiveServerTestCase):

    '''def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')'''

    def setUp(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--dns-prefetch-disable')
        self.options.add_argument('--no-sandbox')
        self.options.add_argument('--headless')
        self.options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=self.options, executable_path='chromedriver')
        self.browser.get(self.live_server_url)
        time.sleep(2)

    def testIndexPageHasTitle(self):
        browser = self.browser
        self.assertIn('', self.browser.title)

    def testOpenAccordion(self):
        browser = self.browser
        accordion = browser.find_element_by_class_name('accordion')
        accordion.send_keys(Keys.RETURN)

    

    