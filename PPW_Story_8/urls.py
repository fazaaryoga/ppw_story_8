from django.urls import path
from . import views

app_name = 'PPW_Story_8'

urlpatterns = [
    path('', views.index, name='index')
]