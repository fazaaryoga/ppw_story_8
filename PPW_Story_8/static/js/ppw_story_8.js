var acc = document.getElementsByClassName('accordion');

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener('click', function () {
        this.classList.toggle('active');

        var panel = this.parentElement.nextElementSibling;
        if (panel.style.display == 'block') {
            panel.style.display = 'none';
        } else {
            panel.style.display = 'block';
        }

    })
}

var swapDown = document.getElementsByClassName('swapDown');

for(i = 0; i < acc.length; i++){
    swapDown[i].addEventListener('click', function() {
        var parent = this.parentElement.parentElement;
        var parentSibling = this.parentElement.parentElement.nextElementSibling;
        swapElements(parent, parentSibling)
    })
}

var swapUp = document.getElementsByClassName('swapUp');

for(i = 0; i < acc.length; i++){
    swapUp[i].addEventListener('click', function() {
        var parent = this.parentElement.parentElement;
        var parentSibling = this.parentElement.parentElement.previousElementSibling;
        swapElements(parentSibling, parent)
    })
}

function swapElements(obj1, obj2){
    var temp = document.createElement('div');
    obj1.parentNode.insertBefore(temp, obj1);
    obj2.parentNode.insertBefore(obj1, obj2);
    temp.parentNode.insertBefore(obj2, temp);
    temp.parentNode.removeChild(temp)
}

